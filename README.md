# Voyager Portal Application Documentation

---

## 1. **Overview**

This application consists of:

-   **Frontend**: Built with React and Tailwind CSS, providing a user-friendly interface.
-   **Backend**: Developed with Express, managing business logic and connecting to a MongoDB database.
-   Both services are orchestrated with **Docker Compose** for seamless containerized execution.

---

## 2. **Prerequisites**

Ensure you have the following installed on your system:

-   [Docker](https://www.docker.com/)
-   [Docker Compose](https://docs.docker.com/compose/)

---

## 3. **How to Run the Application**

1. Clone the repository to your local machine:
    ```bash
    git clone <REPOSITORY_URL>
    cd voyager
    ```
2. Make sure Docker is running on your system.

3. Build and start the containers by running:

    ```bash
    docker-compose up --build
    ```

4. Access the application in your browser:
   Frontend: http://localhost:3000
   Backend: http://localhost:5000

5. To stop the containers, run:

    ```bash
    docker-compose down
    ```

## 4. **Project Structure**

### Main Directories:

-   **`frontend/`**: Contains the React client code.
    -   **`src/`**: Main frontend source code.
    -   **`public/`**: Static assets and public files.
-   **`backend/`**: Express server code.
    -   **`domain/`**: Business logic and validations.
    -   **`infrastructure/`**: MongoDB database configuration.
    -   **`application/`**: Server controllers and routes.
-   **Docker Files**:
    -   `docker-compose.yml`: Orchestration configuration.
    -   `Dockerfile` (in both directories): Container configuration.
