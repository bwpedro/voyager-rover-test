const express = require('express');
const bodyParser = require('body-parser');
const router = require('./application/routes/routes');
const cors = require('cors');
const connectToDatabase = require('./infrastructure/database');

const app = express();

connectToDatabase();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(router);

app.listen(3001, () => {
	console.log('Server is listening');
});
