// robot orientations
const L = 'L';
const R = 'R';
const M = 'M';

// cardinal compass points
const N = 'N';
const S = 'S';
const E = 'E';
const W = 'W';

module.exports = {
	L,
	R,
	M,
	N,
	S,
	E,
	W,
};
