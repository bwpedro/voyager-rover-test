const Robot = require('../models/RobotSchema');

const saveCoordinates = async (data) => {
	try {
		const robot = new Robot(data);
		const savedRobot = await robot.save();

		console.log('Robot saved:', savedRobot);

		return savedRobot;
	} catch (error) {
		console.error('Error saving robot:', error);
		throw new Error('Failed to save robot');
	}
};

module.exports = {
	saveCoordinates,
};
