const test = require('tape');
const { N, W, S, E } = require('../../constants');
const { getFinalCoordinates } = require('../index');

console.log('## ROBOT TESTS START ##');

test('Testing getFinalCoordinates() with initial coordinates 1 2 N and LMLMLMLMM instructions', (t) => {
	t.assert(
		JSON.stringify(getFinalCoordinates('1 2 N', 'LMLMLMLMM')) ===
			JSON.stringify([1, 3, N]),
		'test IS DONE!'
	);
	t.end();
});

test('Testing getFinalCoordinates() with initial coordinates 3 3 E and MRRMMRMRRM instructions', (t) => {
	t.assert(
		JSON.stringify(getFinalCoordinates('3 3 E', 'MRRMMRMRRM')) ===
			JSON.stringify([2, 3, S]),
		'test IS DONE!'
	);
	t.end();
});

test('Testing getFinalCoordinates() with initial coordinates 0 0 S and MRMLMMMRMRRM instructions', (t) => {
	t.assert(
		JSON.stringify(getFinalCoordinates('0 0 S', 'MRMLMMMRMRRM')) ===
			JSON.stringify([-1, -4, E]),
		'test IS DONE!'
	);
	t.end();
});

test('Testing getFinalCoordinates() with initial coordinates 0 0 S and MRMLMMMRMRRM instructions', (t) => {
	t.assert(
		JSON.stringify(getFinalCoordinates('8 3 E', 'MMLMMMRMRRM')) ===
			JSON.stringify([10, 6, W]),
		'test IS DONE!'
	);
	t.end();
});
