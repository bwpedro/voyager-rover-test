const { L, R, M } = require('../constants');
const getNewOrientation = require('./helper/orientation');
const getNewCoordinate = require('./helper/coordinate');

class Robot {
	constructor(initialCoordinates, instructions) {
		this.initialCoordinates = initialCoordinates;
		this.instructions = instructions;
	}

	getFinalCoordinates() {
		let [x, y, orientation] = this.initialCoordinates.split(' ');
		x = parseInt(x);
		y = parseInt(y);

		for (const instruction of this.instructions) {
			if (instruction === L || instruction === R) {
				orientation = getNewOrientation(instruction, orientation);
			} else if (instruction === M) {
				[x, y] = getNewCoordinate(x, y, orientation);
			}
		}

		return [x, y, orientation].join(' ');
	}
}

module.exports = Robot;
