const directionOffsets = {
	N: [0, 1],
	E: [1, 0],
	S: [0, -1],
	W: [-1, 0],
};

function getNewCoordinate(x, y, orientation) {
	const [dx, dy] = directionOffsets[orientation];

	return [x + dx, y + dy];
}

module.exports = getNewCoordinate;
