const test = require('tape');
const { N, W, S, E, L, R } = require('../../../constants');
const getNewCoordinate = require('../coordinate');
const getNewOrientation = require('../orientation');

console.log('## SERVICES TESTS START ##');

test('Testing getNewCoordinate() with robot coordinates (0, 0) and N orientation', (t) => {
	t.assert(
		JSON.stringify(getNewCoordinate(0, 0, N)) === JSON.stringify([0, 1]),
		'test IS DONE!'
	);
	t.end();
});

test('Testing getNewCoordinate() with robot coordinates (0, 0) and W orientation', (t) => {
	t.assert(
		JSON.stringify(getNewCoordinate(0, 0, W)) === JSON.stringify([-1, 0]),
		'test IS DONE!'
	);
	t.end();
});

test('Testing getNewCoordinate() with robot coordinates (0, 0) and S orientation', (t) => {
	t.assert(
		JSON.stringify(getNewCoordinate(0, 0, S)) === JSON.stringify([0, -1]),
		'test IS DONE!'
	);
	t.end();
});

test('Testing getNewCoordinate() with robot coordinates (0, 0) and E orientation', (t) => {
	t.assert(
		JSON.stringify(getNewCoordinate(0, 0, E)) === JSON.stringify([1, 0]),
		'test IS DONE!'
	);
	t.end();
});

test('Testing getNewOrientation() with instruction L and N orientation', (t) => {
	t.assert(getNewOrientation(L, N) === W, 'test IS DONE!');
	t.end();
});

test('Testing getNewOrientation() with instruction L and W orientation', (t) => {
	t.assert(getNewOrientation(L, W) === S, 'test IS DONE!');
	t.end();
});

test('Testing getNewOrientation() with instruction L and S orientation', (t) => {
	t.assert(getNewOrientation(L, S) === E, 'test IS DONE!');
	t.end();
});

test('Testing getNewOrientation() with instruction L and E orientation', (t) => {
	t.assert(getNewOrientation(L, E) === N, 'test IS DONE!');
	t.end();
});

test('Testing getNewOrientation() with instruction R and N orientation', (t) => {
	t.assert(getNewOrientation(R, N) === E, 'test IS DONE!');
	t.end();
});

test('Testing getNewOrientation() with instruction R and W orientation', (t) => {
	t.assert(getNewOrientation(R, W) === N, 'test IS DONE!');
	t.end();
});

test('Testing getNewOrientation() with instruction R and S orientation', (t) => {
	t.assert(getNewOrientation(R, S) === W, 'test IS DONE!');
	t.end();
});

test('Testing getNewOrientation() with instruction R and E orientation', (t) => {
	t.assert(getNewOrientation(R, E) === S, 'test IS DONE!');
	t.end();
});
