const { N, W, S, E, L } = require('../../constants');

const directions = [N, E, S, W];

function getLeftCardinalPoint(currOrientation) {
	const index = directions.indexOf(currOrientation);

	return directions[(index + 3) % 4];
}

function getRightCardinalPoint(currOrientation) {
	const index = directions.indexOf(currOrientation);

	return directions[(index + 1) % 4];
}

function getNewOrientation(instruction, currOrientation) {
	return instruction === L
		? getLeftCardinalPoint(currOrientation)
		: getRightCardinalPoint(currOrientation);
}

module.exports = getNewOrientation;
