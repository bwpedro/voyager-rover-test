const mongoose = require('mongoose');

const RobotSchema = new mongoose.Schema(
	{
		initialCoordinates: {
			type: [String],
			required: true,
		},
		instructions: {
			type: [String],
			required: true,
		},
		finalCoordinates: {
			type: [String],
			required: true,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model('Robot', RobotSchema);
