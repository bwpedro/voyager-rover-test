const { isValidCoordinates, isValidInstructions } = require('../validations');
const Robot = require('../robot/Robot');
const repository = require('../repositories/robot');

async function getCoordinates(initialCoordinates, instructions) {
	if (!initialCoordinates || !instructions) {
		throw new Error(
			'Invalid input: Provide pairs of coordinates and instructions. See the README for details.'
		);
	}

	if (!isValidCoordinates(initialCoordinates)) {
		throw new Error(`Invalid coordinates: ${initialCoordinates}`);
	}

	if (!isValidInstructions(instructions)) {
		throw new Error(`Invalid instructions: ${instructions}`);
	}

	const robot = new Robot(initialCoordinates, instructions);

	await repository.saveCoordinates({
		initialCoordinates,
		instructions,
		finalCoordinates: robot.getFinalCoordinates(),
	});

	return robot.getFinalCoordinates();
}

module.exports = { getCoordinates };
