const test = require('tape');
const { isValidCoordinates, isValidInstructions } = require('../index');

console.log('## VALIDATION TESTS START ##');

test('Testing isValidCoordinates() with 1 2 N', (t) => {
	t.assert(isValidCoordinates('1 2 N') === true, 'test IS DONE!');
	t.end();
});

test('Testing isValidCoordinates() with 1 2 K', (t) => {
	t.assert(isValidCoordinates('1 2 K') === false, 'test IS DONE!');
	t.end();
});

test('Testing isValidCoordinates() with 1 M S', (t) => {
	t.assert(isValidCoordinates('1 M S') === false, 'test IS DONE!');
	t.end();
});

test('Testing isValidInstructions() LMLMLMRMM', (t) => {
	t.assert(isValidInstructions('LMLMLMRMM') === true, 'test IS DONE!');
	t.end();
});

test('Testing isValidInstructions() LMKMLMRGM', (t) => {
	t.assert(isValidInstructions('LMKMLMRGM') === false, 'test IS DONE!');
	t.end();
});
