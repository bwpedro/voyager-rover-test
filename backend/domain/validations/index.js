const { N, W, S, E } = require('../constants');

/**
 * Verifica se as coordenadas fornecidas são válidas.
 * @param {string} coordinates - String no formato "x y orientation"
 * @returns {boolean} - true se as coordenadas são válidas, false caso contrário
 */
function isValidCoordinates(coordinates) {
	const [x, y, orientation] = coordinates.split(' ');

	return (
		!Number.isNaN(Number(x)) &&
		!Number.isNaN(Number(y)) &&
		[N, W, S, E].includes(orientation)
	);
}

/**
 * Verifica se as instruções fornecidas são válidas (contendo apenas "L", "R" ou "M").
 * @param {string} instructions - String com as instruções
 * @returns {boolean} - true se as instruções são válidas, false caso contrário
 */
function isValidInstructions(instructions) {
	return /^(L|R|M)+$/.test(instructions);
}

module.exports = {
	isValidCoordinates,
	isValidInstructions,
};
