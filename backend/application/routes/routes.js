const router = require('express').Router();
const RobotController = require('../controller/RobotController');

const controller = new RobotController();

router.get('/coordinates', controller.getCoordinates);

module.exports = router;
