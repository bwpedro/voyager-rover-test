const robotService = require('../../domain/services/robot');

class RobotController {
	async getCoordinates(request, response) {
		const { query } = request;

		const { initialCoordinates, instructions } = query;

		try {
			const payload = await robotService.getCoordinates(
				initialCoordinates,
				instructions
			);

			response.status(200).json(payload);
		} catch (e) {
			console.log(e);
			response.status(400).json(e.message);
		}
	}
}

module.exports = RobotController;
