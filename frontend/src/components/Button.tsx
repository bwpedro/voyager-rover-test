type Properties = {
	disabled: boolean;
	onClick: () => void;
	children: React.ReactNode;
};

const Button: React.FC<Properties> = ({ disabled, onClick, children }) => {
	return (
		<button
			disabled={disabled}
			onClick={onClick}
			className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-lg shadow"
		>
			{children}
		</button>
	);
};

export default Button;
