type Properties = {
	children: React.ReactNode;
};

const Label: React.FC<Properties> = ({ children }) => {
	return <label className="block font-medium mb-2">{children}</label>;
};

export default Label;
