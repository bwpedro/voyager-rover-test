type Properties = {
	children: React.ReactNode;
};

const Content: React.FC<Properties> = ({ children }) => {
	return (
		<div className="bg-white text-gray-900 p-6 rounded-lg shadow-lg w-full max-w-lg mt-4">
			{children}
		</div>
	);
};

export default Content;
