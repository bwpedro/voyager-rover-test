type Properties = {
	children: React.ReactNode;
};

const Subtitle: React.FC<Properties> = ({ children }) => {
	return <h3 className="text-xl font-semibold mb-4">{children}</h3>;
};

export default Subtitle;
