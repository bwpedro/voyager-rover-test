type Properties = {
	disabled: boolean;
	value: string | number | readonly string[] | undefined;
	onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const Input: React.FC<Properties> = ({ disabled, value, onChange }) => {
	return (
		<input
			disabled={disabled}
			type="text"
			value={value}
			onChange={onChange}
			className="w-full p-2 border rounded-md mb-4 focus:outline-none focus:ring-2 focus:ring-blue-500"
		/>
	);
};

export default Input;
