type Properties = {
	children: React.ReactNode;
};

const PageContainer: React.FC<Properties> = ({ children }) => {
	return (
		<div className="min-h-screen bg-voyagerGreen text-white flex flex-col items-center p-8">
			{children}
		</div>
	);
};

export default PageContainer;
