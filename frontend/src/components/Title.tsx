type Properties = {
	children: React.ReactNode;
};

const Title: React.FC<Properties> = ({ children }) => {
	return <h1 className="text-4xl font-bold mb-6">{children}</h1>;
};

export default Title;
