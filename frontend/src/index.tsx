import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import MarsRover from './pages/mars-rover/MarsRover';

const root = ReactDOM.createRoot(
	document.getElementById('root') as HTMLElement
);

root.render(
	<React.StrictMode>
		<MarsRover />
	</React.StrictMode>
);
