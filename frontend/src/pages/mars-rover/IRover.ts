export default interface IRover {
	position: string;
	instructions: string;
}
