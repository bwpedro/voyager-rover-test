import { useState } from 'react';
import IRover from './IRover';
import Content from '../../components/Content';
import Title from '../../components/Title';
import PageContainer from '../../components/PageContainer';
import Label from '../../components/Label';
import Input from '../../components/Input';
import Subtitle from '../../components/Subtitle';
import RobotGrid from './components/RobotGrid';
import getCoordinates from '../../requests/getCoordinates';
import Button from '../../components/Button';
import Alert from '../../components/Alert';

const MarsRover = () => {
	const [rover, setRover] = useState<IRover>({
		position: '',
		instructions: '',
	});
	const [finalPosition, setFinalPosition] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const [errorMessage, setErrorMessage] = useState<string | null>(null);

	const handlePosition = (value: string) => {
		setRover((prev) => ({
			...prev,
			position: value,
		}));
	};

	const handleInstructions = (value: string) => {
		setRover((prev) => ({
			...prev,
			instructions: value,
		}));
	};

	const handleSubmit = async () => {
		setIsLoading(true);

		await getCoordinates(rover, (result, error) => {
			setIsLoading(false);

			if (result) {
				setFinalPosition(result);
			}

			if (error) {
				setErrorMessage(error);
			}
		});
	};

	return (
		<PageContainer>
			<Title>Voyager Portal Assessment</Title>
			{errorMessage != null ? (
				<Alert
					description={errorMessage}
					onClose={() => setErrorMessage(null)}
				/>
			) : null}
			<Content>
				<Subtitle>Rover</Subtitle>
				<Label>Landing Position (e.g., "1 2 N"):</Label>
				<Input
					disabled={isLoading}
					value={rover.position}
					onChange={(e) => handlePosition(e.target.value)}
				/>
				<Label>Instructions (e.g., "LMLMLMLMM"):</Label>
				<Input
					disabled={isLoading}
					value={rover.instructions}
					onChange={(e) => handleInstructions(e.target.value)}
				/>
				<div className="flex float-right space-x-4 mt-6">
					<Button disabled={isLoading} onClick={handleSubmit}>
						Compute Final Positions
					</Button>
				</div>
			</Content>

			<Content>
				<Subtitle>Result</Subtitle>
				<RobotGrid finalPosition={finalPosition} rover={rover} />
			</Content>
		</PageContainer>
	);
};

export default MarsRover;
