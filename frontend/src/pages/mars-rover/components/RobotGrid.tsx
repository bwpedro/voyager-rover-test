import { useState } from 'react';
import IRover from '../IRover';

interface IRobotGrid {
	finalPosition: string;
	rover: IRover;
}

const RobotGrid: React.FC<IRobotGrid> = ({ finalPosition, rover }) => {
	const [gridDimensions, _] = useState({ x: 5, y: 5 });

	const showArrow = (orientation: string) => {
		switch (orientation) {
			case 'N':
				return '↑';
			case 'S':
				return '↓';
			case 'E':
				return '→';
			case 'W':
				return '←';
			default:
				break;
		}
	};

	const parseFinalPosition = (position: string) => {
		const [x, y, orientation] = position.split(' ');
		return { x: parseInt(x), y: parseInt(y), orientation };
	};

	const parseLandingPosition = (position: string) => {
		const [x, y, orientation] = position.split(' ');
		return { x: parseInt(x), y: parseInt(y), orientation };
	};

	const renderGrid = () => {
		const {
			x: finalX,
			y: finalY,
			orientation,
		} = parseFinalPosition(finalPosition);

		const {
			x: landingX,
			y: landingY,
			orientation: landingOrientation,
		} = parseLandingPosition(rover.position);

		const grid = [];
		for (let i = 1; i <= gridDimensions.y; i++) {
			const row = [];
			for (let j = 1; j <= gridDimensions.x; j++) {
				if (i === finalY && j === finalX) {
					row.push(
						<div
							key={`cell-${i}-${j}`}
							className={`w-12 h-12 flex items-center justify-center bg-gray-300 border border-gray-600`}
						>
							<span className="text-xl">
								{showArrow(orientation)}
							</span>
						</div>
					);
				} else if (i === landingY && j === landingX) {
					row.push(
						<div
							key={`cell-${i}-${j}`}
							className={`w-12 h-12 flex items-center justify-center bg-blue-300 border border-gray-600`}
						>
							<span className="text-xl">
								{showArrow(landingOrientation)}
							</span>
						</div>
					);
				} else {
					row.push(
						<div
							key={`cell-${i}-${j}`}
							className={`w-12 h-12 flex items-center justify-center bg-gray-100 border border-gray-600`}
						/>
					);
				}
			}
			grid.push(
				<div key={`row-${i}`} className="flex">
					{row}
				</div>
			);
		}

		return grid;
	};

	return (
		<>
			<p className="mb-2">
				<span className="font-bold">Final position:</span>{' '}
				{finalPosition}
			</p>
			{renderGrid()}
		</>
	);
};

export default RobotGrid;
