import IRover from '../pages/mars-rover/IRover';
import { BASE_URL } from './constants';

export default async function getCoordinates(
	input: IRover,
	callback: (result: string | null, error: string | null) => void
): Promise<void> {
	try {
		const response = await fetch(
			`${BASE_URL}/coordinates?initialCoordinates=${input.position}&instructions=${input.instructions}`
		);

		const data = await response.json();

		if (response.ok) {
			callback(data, null);
		} else {
			callback(null, data);
		}
	} catch (e) {
		callback(null, "Error to connect to the server. Check if it's on.");
	}
}
