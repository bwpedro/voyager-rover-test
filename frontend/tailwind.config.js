/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        voyagerGreen: "#3da67f",
      },
    },
  },
  plugins: [],
};
